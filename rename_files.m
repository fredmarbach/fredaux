% rename files that are saves as ____1.tif ... ____10.tif and sort badly
% to ____001.tif etc.


function [] = rename_files(files)


for ff = 1:length(files)
    % get number at the end
    thisName = files(ff).name;
    tmp = strfind(thisName,'_');
    thisNumber = thisName(tmp(end)+1:end-4);
    newNumber = sprintf('%03d.tif',str2double(thisNumber));
    newName = [thisName(1:tmp(end)) newNumber];
    oldPath = fullfile(files(ff).folder,thisName);
    newPath = fullfile(files(ff).folder,newName);
    if ~strcmp(thisName,newName)
        movefile(oldPath,newPath);
    end
end  
