% given a set of indices 'inds' and a max length 'fullLength'
% returns a logical index vector of length 'fullLength' with true for
% indices in 'inds'

function out = getLogical(inds,fullLength)
    out = false(1,fullLength);
    out(inds) = true;
