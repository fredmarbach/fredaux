% get google sheet info
% essentially serves as lookup table, all 'hard coded'
% add mice/tabs as they are created
% input:
%     sheetName - google sheet name
%      pageName - sub-sheet/tab name
% output:
%    gSheet.DOCID - see below
%      gSheet.GID - see below
% https://docs.google.com/spreadsheets/d/DOCID/edit#gid=GID
% to be run before getGoogleSpreadsheet(DOCID,GID) from fileexchange
% note: need to modify getGoogleSpreadsheet(DOCID) to take GID as input as
% suggested in the comments on filexchange

function gSheet = getGSheetString(sheetName,pageName)

switch sheetName
    case 'Experiment Log'
        gSheet.DOCID = '12bwrZnE7KhJKMBzWDyIQVENkVTlSgvYyzSi_wquCne0';
        GID_tab = '1948589301'; % contains list of GIDs for all tabs

end

allTabs = GetGoogleSpreadsheet(gSheet.DOCID,GID_tab);

% fetch the correct GID for the desired page/tab
tabNames = allTabs(1:end,1);
thisTab = strcmp(tabNames,pageName);
gSheet.GID = allTabs(thisTab,2);





% old approach with .m file:

% switch pageName
%     case 'muscimol'
%         gSheet.GID = '0';
%     case 'mar036'
%         gSheet.GID = '145593657';
%     case 'mar037'
%         gSheet.GID = '330241455';
%     case 'mar038'
%         gSheet.GID = '1049922284';
%     case 'mar052'
%         gSheet.GID = '60671720';
%     case 'mar053'
%         gSheet.GID = '1503289818';
%     case 'mar065'
%         gSheet.GID = '1908383688';
%     case 'mar066'
%         gSheet.GID = '1710304915';
%     case 'mar068'
%         gSheet.GID = '709848642';
%     case 'mar069'
%         gSheet.GID = '67085886';
%     case 'mar071'
%         gSheet.GID = '1522548997';
%     case 'mar072'
%         gSheet.GID = '1514878895';
%     case 'mar073'
%         gSheet.GID = '676912372';
%     case 'mar074'
%         gSheet.GID = '1766221337';
%     case 'mar075'
%         gSheet.GID = '1890764877';
%     case 'mar104'
%         gSheet.GID = '570218078';
%     case 'mar106'
%         gSheet.GID = '387168261';
% 
% end