% do some basic formatting
% formatFigure(hf,ha,textSize,lineWidth,markerSize,...
%              xlab,ylab,xlimit,ylimit,...
%              tickDirOut,pbasp,figureSize,boxON,whiteBckg)

function [] = formatFigure(hf,ha,textSize,lineWidth,markerSize,...
                           xlab,ylab,xlimit,ylimit,...
                           tickDirOut,pbasp,figureSize,boxON,whiteBckg)

for ii = 1:numel(hf)
    figure(hf(ii));
    if length(figureSize) > 1
        set(hf, 'units', 'inches', 'position', figureSize)
    end

    if whiteBckg
        set(hf,'color','white')
    end
end

for jj = 1:numel(ha)
    axes(ha(jj));
    if length(xlimit) > 1
        try xlim(xlimit); catch end
    end

    if length(ylimit) > 1
        try ylim(ylimit); catch end
    end

    if lineWidth > 0
        %set(hf, 'LineWidth', lineWidth);
    end
    
    if boxON == 1
        box on;
    else
       box off;
    end
    
    if tickDirOut
        ha(jj).TickDir = 'out';
    end

%     if markerSize > 0
%         ha(jj).MarkerSize = markerSize;
%     end

    if xlab ~= 0
        if textSize ~= 0
            xlabel(ha(jj), xlab, 'FontSize', textSize);
        else
            xlabel(ha(jj), xlab);
        end
    end
    if ylab ~=0
        if textSize ~= 0
            ylabel(ha(jj), ylab, 'FontSize', textSize);
        else
            ylabel(ha(jj), ylab);
        end
    end
    if textSize ~= 0
        set(ha(jj), 'Fontsize', textSize);
    end

    if length(pbasp) > 1
        pbaspect(ha(jj), pbasp);
    end
    
    % set axis color to black
    ha(jj).XColor = 'k'; 
    ha(jj).YColor = 'k';
end

    




%rotateXLabels(gca, 45);
%set(gca, 'XTick', xVec, 'XTickLabel', mouseNames, 'FontSize', textSize);