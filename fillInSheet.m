% read folder contents of Bpod_Data, one mouse
% fill out google spreadsheet with dates and filenames
% note: need to have created a sheet/tab for this mouse and entered ID into
% getGSheetString.m

% --- parameters to set ---

mouse = 'mar072';

rootFolder = '/Volumes/winstor/swc/sjones/users/fred/Data_Bpod/';
protocol = 'BeliefState';
dataFolder = fullfile(rootFolder,mouse,protocol,'Session Data');

% =========================

% ---------------------------------
% get google sheet info for this mouse
% this is a fred function, see above for instructions
gSheet = getGSheetString('Experiment Log',mouse);

% get all files ending with .mat
files = dir(fullfile(dataFolder,'*.mat'));

% make title row
titles = {'mouse' 'date' 'file name' 'day' 'include'};
mat2sheets(gSheet.DOCID,gSheet.GID,[1 1],titles);

% select largest file per day
dateInd = length(mouse) + length(protocol) + 3;
[~,dates] = convertBpodDate({files.name},dateInd);

% pick largest data file per day
ind = 1;
maxInd = [];
day = {}; mouseRep = {};
for dd = 1:length(unique(dates))
    thisDate = dates{ind};
    sameInd = strcmp(dates,thisDate);
    [~,maxInd(dd)] = max([files.bytes] .* sameInd);
    ind = find(sameInd,1,'last')+1;
    day{dd} = num2str(dd);
    mouseRep{dd} = mouse;
end

% sort chosen files according to actual date
chosenFiles = {files(maxInd).name}
chosenFiles_noext = cellfun(@(x) x(1:end-4),chosenFiles,'UniformOutput',false);
chosenDates = dates(maxInd);
[~,sortInd] = sort(chosenDates);

% write data to sheet
mat2sheets(gSheet.DOCID,gSheet.GID,[2 1],mouseRep');
mat2sheets(gSheet.DOCID,gSheet.GID,[2 2],chosenDates(sortInd)');
mat2sheets(gSheet.DOCID,gSheet.GID,[2 3],chosenFiles_noext(sortInd)');
mat2sheets(gSheet.DOCID,gSheet.GID,[2 4],day');








