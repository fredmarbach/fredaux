% function handles pause until GUI is filled in
% opens a figure window that has to be clicked to continue
% input:
%   size_xy - figure dimensions in inches
%   location - 'northwest' (default) etc.

function [] = pauseUntilClick(size_xy,varargin)

if nargin == 2
    location = varargin{1};
else
    location = 'northwest';
end

dummyFigure = figure('Units','Inches','Position',[1 1 size_xy(1) size_xy(2)],...
                     'MenuBar','none');

movegui(dummyFigure,location)

title('Click here to START');

waitforbuttonpress; 

close(dummyFigure);
    