% useful code snippets to remember

% ------------------------
% --- plotting related ---
% ------------------------

print(gcf,'-depsc','-painters',fname) 

rectangle('Position',[stimTime(1) min(tmp_data(:)) ...
                                  stimTime(2)-stimTime(1) ...
                                  max(tmp_data(:))-min(tmp_data(:))],...
                      'FaceColor',0.85*[1 1 1],'EdgeColor','none');
                  
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.24, 1, 0.66]);

set(groot,'DefaultAxesColorOrder',summer(5,1))); 

xticklabels = timePrePost(1):1:timePrePost(2);
xticks = linspace(1, size(meanData, 2), numel(xticklabels));
set(gca, 'XTick', xticks, 'XTickLabel', xticklabels)

uistack(tmp_h,'bottom')

h_line = findobj(gca,'Type','line');
h_line.XData

brewermap('demo')
colormap(brewermap([],'*RdYlBu'))

bcolors = ...
    [128 128 255;... % fade purple
     128 00 255;...  % purple pink sat
     192 192 192;... % light grey
     64 128 255; ... % light blue unsat
     192 255 00; ... % green yellow sat
     00 128 00;...   % dark green
     192 64 00; ...  % brick
     255 128 00; ... % apricot
     255 192 00;...  % peach
     192 00 255; ... % hot pink
     64 255 255; ... % cyan
     96 96 96;...    % grey
     ]/255;    

f1 = figure(1);
for ii = 1:size(bcolors,1)
    plot(ii*ones(10,1),'color',bcolors(ii,:),'linewidth',3);
    hold on;
end
f1.Color = [0 0 0];
set(gca,'xcolor',0.85*[1 1 1],'ycolor',0.85*[1 1 1])
set(gca,'Color',[0 0 0]);
    

axis tight
lim = eqAx('none'); 
        
        
% ------------------------
% --- stats --------------
% ------------------------
ci = bootci(1000,@mean,thisData);

filtfilt(ones(1,smoothW)/smoothW,1,thisTrace)

































