% get the min and max axes values 
% sets 'axis tight' and uses get(ax,'xlim')

function lim = getDataLimits(axesHandle)

axes(axesHandle);
axis tight;

lim.x = get(axesHandle,'xlim');
lim.y = get(axesHandle,'ylim');