% compares two structures and matches fields 
% this is a recursive function
% input:
%   A - struct of interest
%   B - reference struct to be matched
%   action - 'copy' copies from B that are missing in A
%          - 'remove' removes fields in A that are missing in B
% output: 
%   A - now modified accordingly

function A = matchFields(A,B,action)

switch action
    
    case 'copy'
        %B.
        names = fieldnames(B);
        for ff = 1:length(names)
            if isfield(A,names{ff})
                if isstruct(B.(names{ff})) 
                    %B.x
                    % use recursion
                    A.(names{ff}) = matchFields(A.(names{ff}),B.(names{ff}),'copy');
                end % no action (A has the field already and there are no deeper fields)
            else % A does not have this field, copy it over
                A.(names{ff}) = B.(names{ff});
            end
        end
        
    case 'remove'
        %A.
        names = fieldnames(A);
        for ff = 1:length(names)
            if isfield(B,names{ff})
                if isstruct(A.(names{ff})) 
                    %A.x
                    % use recursion
                    A.(names{ff}) = matchFields(A.(names{ff}),B.(names{ff}),'remove');
                end % no action
            else % B does not have this field, remove it from A
                A = rmfield(A,names{ff});
            end
        end
end









