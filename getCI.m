% calculate confidence interval given a set of values and alpha

function CI = getCI(data, a, relativeToMean)

u = 1-a/2;
l = a/2;
    
if length(size(data)) == 1

    ds = sort(data);
    x = linspace(0, 1, length(ds));
    xu = pchip(x,ds,u);
    xl = pchip(x,ds,l);
    CI = [xl xu];

    % figure; hold on;
    % plot(ds, x, 'o');
    % line([xu xu], [0 1]);
    % line([xl xl], [0 1]);
else
    % data is trials x time
    ds = sort(data, 1);
    CI = zeros(2, size(data, 2));
    for ii = 1:size(data, 2) % loop over time
        tmp = ds(:, ii); % all trials, one time point
        x = linspace(0, 1, length(tmp));
        xu = pchip(x,tmp,u);
        xl = pchip(x,tmp,l);
        CI(:, ii) = [xu xl];
    end
end

if relativeToMean
    CI = bsxfun(@minus, CI, mean(data, 1));
end


